package ru.zvg.footballrate;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.StrictMode;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener, IPrefsKeys {

    private NavController navController;
    private Game selectedGame;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.INTERNET},
                PackageManager.PERMISSION_GRANTED);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        SQLDataBase.createDataBase(this);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        prefs = getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        initializeAccount();
    }

    public void initializeAccount() {
        if (!prefs.contains(EMAIL_KEY)) {
            return;
        }
        String login = prefs.getString(LOGIN_KEY, "");
        String email = prefs.getString(EMAIL_KEY, "");
        Account.initializeAcc(this, login, email);
    }

    public Game getSelectedGame() {
        return selectedGame;
    }

    public void setSelectedGame(Game selectedGame) {
        this.selectedGame = selectedGame;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.getItem(0);
        item.setOnMenuItemClickListener(this);
        return true;
    }


    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if (Account.getAccount() != null) {
            setRenderFragment(R.id.nav_main_cabinet);
            return true;
        }
        setRenderFragment(R.id.nav_authorization);
        return true;
    }

    public void setRenderFragment(int id) {
        navController.navigate(id);
    }
}
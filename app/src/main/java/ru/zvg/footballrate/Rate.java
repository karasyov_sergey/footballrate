package ru.zvg.footballrate;

public class Rate {

    private Team team;
    private int value;
    private int game;

    public Rate(int game, int value, Team team) {
        this.game = game;
        this.team = team;
        this.value = value;
    }

    public Team getTeam() {
        return team;
    }

    public int getValue() {
        return value;
    }

    public int getGame() {
        return game;
    }
}

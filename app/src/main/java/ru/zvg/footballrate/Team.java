package ru.zvg.footballrate;

public class Team {
    private League league;
    private String name;
    private byte[] logo;

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }
}

package ru.zvg.footballrate;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Locale;

public class GameFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    private EditText rateET;
    private TextView teamLeft;
    private TextView teamRight;
    private TextView countLeft;
    private TextView countRight;
    private TextView leagueLeft;
    private TextView leagueRight;
    private ImageView teamLeftLogo;
    private ImageView teamRightLogo;
    private ImageView leagueLeftLogo;
    private ImageView leagueRightLogo;
    private Game game;
    private Team selectedTeam;
    private Account account;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        if (activity == null) {
            ToastCreator.showToast(view.getContext(), R.string.fatal_error);
            return;
        }
        game = activity.getSelectedGame();
        if (game == null) {
            ToastCreator.showToast(view.getContext(), R.string.fatal_error);
            return;
        }
        account = Account.getAccount();
        initializeUI(view);
        bindDataToUI();
    }

    private void bindDataToUI() {
        this.bindTextData(teamLeft, game.getTeamLeft().getName());
        bindTextData(teamRight, game.getTeamRight().getName());
        bindTextData(countLeft, game.getCountLeft());
        bindTextData(countRight, game.getCountRight());
        bindTextData(leagueLeft, game.getTeamLeft().getLeague().getName());
        bindTextData(leagueRight, game.getTeamRight().getLeague().getName());
        bindImgData(teamLeftLogo, game.getTeamLeft().getLogo());
        bindImgData(teamRightLogo, game.getTeamRight().getLogo());
        bindImgData(leagueLeftLogo, game.getTeamLeft().getLeague().getLogo());
        bindImgData(leagueRightLogo, game.getTeamRight().getLeague().getLogo());
    }

    private void bindImgData(ImageView imageView, byte[] data) {
        Glide.with(imageView.getContext()).asBitmap().load(data).into(imageView);
        if (imageView.getId() == R.id.team_left_logo || imageView.getId() == R.id.team_right_logo) {
            Log.d("byte data", Arrays.toString(data));
        }
    }

    private void bindTextData(TextView textView, Object object) {
        textView.setText(object.toString());
    }

    private void initializeUI(View view) {
        initializeTextView(view);
        initializeImageView(view);
        initializeRadioView(view);
        initializeEditView(view);
        initializeButtonView(view);
    }

    private void initializeEditView(View view) {
        rateET = view.findViewById(R.id.rate_edit);
    }

    private void initializeButtonView(View view) {
        Button btnDoRate = view.findViewById(R.id.btn_do_rate);
        btnDoRate.setOnClickListener(this);
    }

    private void initializeRadioView(View view) {
        RadioGroup radioGroup = view.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(this);
    }

    private void initializeTextView(View view) {
        teamLeft = view.findViewById(R.id.team_left);
        teamRight = view.findViewById(R.id.team_right);
        countLeft = view.findViewById(R.id.count_left);
        countRight = view.findViewById(R.id.count_right);
        leagueLeft = view.findViewById(R.id.league_left);
        leagueRight = view.findViewById(R.id.league_right);
    }

    private void initializeImageView(View view) {
        teamLeftLogo = view.findViewById(R.id.team_left_logo);
        teamRightLogo = view.findViewById(R.id.team_right_logo);
        leagueLeftLogo = view.findViewById(R.id.league_left_logo);
        leagueRightLogo = view.findViewById(R.id.league_right_logo);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.radio_btn_team_left:
                selectedTeam = game.getTeamLeft();
                break;
            case R.id.radio_btn_team_right:
                selectedTeam = game.getTeamRight();
                break;
            default:
        }
    }

    @Override
    public void onClick(View view) {
        String rateEntered = rateET.getText().toString();
        if (rateEntered.trim().isEmpty()) {
            ToastCreator.showToast(view.getContext(), R.string.enter_rate);
            return;
        }
        if (selectedTeam == null) {
            ToastCreator.showToast(view.getContext(), R.string.selected_team_is_null);
            return;
        }
        int rate = Integer.parseInt(rateEntered);
        if (rate <= 0) {
            ToastCreator.showToast(view.getContext(), R.string.invalid_rate);
            return;
        }
        if (account == null) {
            ToastCreator.showToast(view.getContext(), R.string.no_log_in);
            return;
        }
        addRate(view.getContext(), rate);
    }

    private void addRate(Context context, int rate) {
        try (Connection connection = SQLDataBase.getConnection()) {
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format(Locale.ENGLISH,
                    SQLDataBase.ADD_RATE, account.getEmail(), rate, game.getId(), selectedTeam.getName()));
            ToastCreator.showToast(context, R.string.successfully_rate);
            Rate rateObj = new Rate(game.getId(), rate, selectedTeam);
            account.addRate(rateObj);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastCreator.showToast(context, R.string.connection_exception);
        }
    }
}
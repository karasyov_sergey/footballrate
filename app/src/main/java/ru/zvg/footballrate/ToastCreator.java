package ru.zvg.footballrate;

import android.content.Context;
import android.widget.Toast;

public class ToastCreator {
    public static void showToast(Context context, int stringRes) {
        Toast.makeText(context, context.getString(stringRes), Toast.LENGTH_LONG).show();
    }
}

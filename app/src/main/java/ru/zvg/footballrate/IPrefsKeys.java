package ru.zvg.footballrate;

public interface IPrefsKeys {
    String PREF_KEY = "Main";
    String LOGIN_KEY = "Login";
    String EMAIL_KEY = "Email";
}

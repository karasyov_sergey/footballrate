package ru.zvg.footballrate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainCabinetFragment extends Fragment implements View.OnClickListener, IPrefsKeys {
    private SQLDataBase dataBase;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.main_cabinet_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataBase = SQLDataBase.getDb();
        Account account = Account.getAccount();
        List<Rate> rateList = Account.getAccount().getRateList();
        List<Game> gameList = takeGameList(rateList);
        RecyclerView recyclerView = view.findViewById(R.id.rate_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(new RateAdapter(rateList, gameList));
        TextView accName = view.findViewById(R.id.welcome_tv);
        accName.setText(String.format(Locale.ENGLISH, getString(R.string.welcome_msg), account.getLogin()));
        Button logOutBtn = view.findViewById(R.id.log_out_btn);
        logOutBtn.setOnClickListener(this);
    }

    private List<Game> takeGameList(List<Rate> rateList) {
        List<Game> gameList = new ArrayList<>();
        for (Rate aRate :
                rateList) {
            gameList.add(dataBase.getGame(aRate.getGame()));
        }
        return gameList;
    }

    @Override
    public void onClick(View view) {
        SharedPreferences prefs = view.getContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().clear().apply();
        Account.clear();
        MainActivity activity = (MainActivity) getActivity();
        if (activity == null) {
            ToastCreator.showToast(view.getContext(), R.string.fatal_error);
            return;
        }
        activity.setRenderFragment(R.id.nav_game_list);
        ToastCreator.showToast(view.getContext(), R.string.log_out_msg);
    }
}

package ru.zvg.footballrate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

public class RateAdapter extends RecyclerView.Adapter<RateAdapter.RateView> {

    private List<Rate> rateList;
    private List<Game> gameList;

    public RateAdapter(List<Rate> rateList, List<Game> gameList) {
        this.rateList = rateList;
        this.gameList = gameList;
    }

    @NonNull
    @Override
    public RateView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rate_element, parent, false);
        return new RateView(view);
    }

    @Override
    public int getItemCount() {
        return rateList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RateView holder, int position) {
        Game game = gameList.get(position);
        Rate rate = rateList.get(position);
        holder.setData(game, rate);
    }

    public class RateView extends RecyclerView.ViewHolder {

        private TextView placeHolder;
        private TextView dateHolder;
        private TextView timeHolder;
        private TextView rateHolder;
        private TextView teamHolder;
        private Context context;
        private Game game;
        private Rate rate;

        public RateView(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            placeHolder = itemView.findViewById(R.id.place_holder);
            dateHolder = itemView.findViewById(R.id.date_holder);
            timeHolder = itemView.findViewById(R.id.time_holder);
            rateHolder = itemView.findViewById(R.id.rate_holder);
            teamHolder = itemView.findViewById(R.id.team_holder);
        }

        public void setData(Game game, Rate rate) {
            this.game = game;
            this.rate = rate;
            bindData();
        }

        private void bindData() {
            String placeData = String.format(Locale.ENGLISH, context.getString(R.string.place), game.getPlace());
            bindTextData(placeHolder, placeData);
            String dateData = String.format(Locale.ENGLISH, context.getString(R.string.date), game.getDate());
            bindTextData(dateHolder, dateData);
            String timeData = String.format(Locale.ENGLISH, context.getString(R.string.time), game.getTime());
            bindTextData(timeHolder, timeData);
            String rateData = String.format(Locale.ENGLISH, context.getString(R.string.rate_value), rate.getValue());
            bindTextData(rateHolder, rateData);
            String teamData = String.format(Locale.ENGLISH, context.getString(R.string.team), rate.getTeam().getName());
            bindTextData(teamHolder, teamData);
        }
    }

    private void bindTextData(TextView textView, String content) {
        textView.setText(content);
    }
}

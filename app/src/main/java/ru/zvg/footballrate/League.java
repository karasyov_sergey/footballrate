package ru.zvg.footballrate;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class League {
    private String name;
    private String country;
    private List<Team> teamList;
    private byte[] logo;

    public League(String name, String country) {
        this.name = name;
        this.country = country;
        teamList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    @Nullable
    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public void addTeam(Team team) {
        teamList.add(team);
        team.setLeague(this);
    }
}

package ru.zvg.footballrate;

import java.sql.Date;
import java.sql.Time;

public class Game {
    private int id;
    private Time time;
    private Date date;
    private String place;
    private Team teamLeft;
    private Team teamRight;
    private int countLeft;
    private int countRight;

    public Game(int id, Team teamLeft, Team teamRight, String place) {
        this.id = id;
        this.teamLeft = teamLeft;
        this.teamRight = teamRight;
        this.place = place;
    }

    public int getId() {
        return id;
    }

    public Time getTime() {
        return time;
    }

    public Date getDate() {
        return date;
    }

    public String getPlace() {
        return place;
    }

    public Team getTeamLeft() {
        return teamLeft;
    }

    public Team getTeamRight() {
        return teamRight;
    }

    public int getCountLeft() {
        return countLeft;
    }

    public int getCountRight() {
        return countRight;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCountLeft(int countLeft) {
        this.countLeft = countLeft;
    }

    public void setCountRight(int countRight) {
        this.countRight = countRight;
    }
}

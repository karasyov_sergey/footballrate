package ru.zvg.footballrate;

import android.content.Context;
import android.util.Log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Account {

    private static Account account;
    private String login;
    private String email;
    private List<Rate> rateList;

    private Account(String login, String email) {
        this.login = login;
        this.email = email;
        rateList = new ArrayList<>();
    }

    private static void buildRateList(Context context) {
        try (Connection connection = SQLDataBase.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH,
                    SQLDataBase.GET_RATE, account.email));
            while (set.next()) {
                Rate rate = new Rate(set.getInt("game"), set.getInt("value"), SQLDataBase.getDb().getTeam(set.getString("team")));
                account.addRate(rate);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastCreator.showToast(context, R.string.connection_exception);
        }
    }

    public static void initializeAcc(Context context, String login, String email) {
        if (account != null) {
            return;
        }
        account = new Account(login, email);
        buildRateList(context);
    }

    public static Account getAccount() {
        return account;
    }

    public void addRate(Rate rate) {
        rateList.add(rate);
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public List<Rate> getRateList() {
        return rateList;
    }

    public static void clear() {
        account = null;
    }
}

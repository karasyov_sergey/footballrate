package ru.zvg.footballrate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class RegistrationFragment extends Fragment implements View.OnClickListener {

    private EditText emailET;
    private EditText passwordET;
    private EditText loginET;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.registration_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailET = view.findViewById(R.id.mail_edit);
        passwordET = view.findViewById(R.id.password_edit);
        loginET = view.findViewById(R.id.login_edit);
        Button regBtn = view.findViewById(R.id.reg_btn);
        regBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!isValidData()) {
            return;
        }
        try (Connection connection = SQLDataBase.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH,
                    SQLDataBase.CONTAINS_ACC, emailET.getText(), passwordET.getText()));
            set.next();
            if (set.getInt("count") != 0) {
                ToastCreator.showToast(view.getContext(), R.string.acc_exists);
                return;
            }
            statement.executeUpdate(String.format(Locale.ENGLISH,
                    SQLDataBase.ADD_ACC, emailET.getText(), loginET.getText(), passwordET.getText()));
            ToastCreator.showToast(view.getContext(), R.string.successfully_reg);
            MainActivity activity = (MainActivity) this.getActivity();
            if (activity == null) {
                ToastCreator.showToast(view.getContext(), R.string.navigation_error);
                return;
            }
            activity.setRenderFragment(R.id.nav_authorization);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastCreator.showToast(view.getContext(), R.string.connection_exception);
        }
    }

    private boolean isValidData() {
        String login = loginET.getText().toString();
        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();
        return isValidPassword(password) || isValidEmail(email) || isValidLogin(login);
    }

    private boolean isValidLogin(String login) {
        if (login == null || login.trim().isEmpty()) {
            loginET.setError(getString(R.string.enter_login));
            return false;
        }
        return true;
    }

    private boolean isValidEmail(String email) {
        if (email == null || email.trim().isEmpty()) {
            emailET.setError(getString(R.string.enter_email));
            return false;
        }
        if (!email.contains("@")) {
            emailET.setError(getString(R.string.invalid_email));
            return false;
        }
        return true;
    }

    private boolean isValidPassword(String password) {
        if (password == null || password.trim().isEmpty()) {
            passwordET.setError(getString(R.string.enter_password));
            return false;
        }
        if (password.length() < 3) {
            passwordET.setError(getString(R.string.too_short_password));
            return false;
        }
        return true;
    }
}

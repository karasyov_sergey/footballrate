package ru.zvg.footballrate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class AuthorizationFragment extends Fragment implements View.OnClickListener, IPrefsKeys {

    private EditText emailET;
    private EditText passwordET;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.authorization_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailET = view.findViewById(R.id.mail_edit);
        passwordET = view.findViewById(R.id.password_edit);
        Button authorizationBtn = view.findViewById(R.id.authorization_btn);
        authorizationBtn.setOnClickListener(this);
        Button toRegBtn = view.findViewById(R.id.to_reg_btn);
        toRegBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.authorization_btn:
                onAuthorizationBtnClick(view.getContext());
                break;
            case R.id.to_reg_btn:
                onToRegBtnClick(view.getContext());
            default:
        }
    }

    private void onToRegBtnClick(Context context) {
        MainActivity activity = (MainActivity) getActivity();
        if (activity == null) {
            ToastCreator.showToast(context, R.string.fatal_error);
            return;
        }
        activity.setRenderFragment(R.id.nav_reg);
    }

    private void onAuthorizationBtnClick(Context context) {
        try (Connection connection = SQLDataBase.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH,
                    SQLDataBase.VALID_ACC, emailET.getText().toString(), passwordET.getText().toString()));
            String login;
            if (!set.next()) {
                emailET.setError(getString(R.string.invalid_log_in_data));
                passwordET.setError(getString(R.string.invalid_log_in_data));
                return;
            }
            login = set.getString("login");
            toCacheAccData(context, login, emailET.getText().toString());
            ToastCreator.showToast(context, R.string.successfully_log_in);
            toMainFragment(context);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastCreator.showToast(context, R.string.connection_exception);
        }
    }

    private void toMainFragment(Context context) {
        MainActivity activity = (MainActivity) getActivity();
        if (activity == null) {
            ToastCreator.showToast(context, R.string.navigation_error);
            return;
        }
        activity.initializeAccount();
        activity.setRenderFragment(R.id.nav_game_list);
    }

    private void toCacheAccData(Context context, String login, String email) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOGIN_KEY, login);
        editor.putString(EMAIL_KEY, email);
        editor.apply();
    }
}

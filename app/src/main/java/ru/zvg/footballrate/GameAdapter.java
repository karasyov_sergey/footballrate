package ru.zvg.footballrate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.GameView> {
    private List<Game> gameList;
    private MainActivity activity;

    public GameAdapter(MainActivity activity, List<Game> gameList) {
        this.gameList = gameList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public GameView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_element, parent, false);
        return new GameView(root, this);
    }

    @Override
    public void onBindViewHolder(@NonNull GameView holder, int position) {
        holder.setGame(gameList.get(position));
    }

    @Override
    public int getItemCount() {
        return gameList.size();
    }

    public MainActivity getActivity() {
        return activity;
    }

    public static class GameView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView placeTV;
        private TextView dateTV;
        private TextView timeTV;
        private Context context;
        private Game game;
        private GameAdapter adapter;

        public GameView(@NonNull View itemView, GameAdapter adapter) {
            super(itemView);
            this.adapter = adapter;
            placeTV = itemView.findViewById(R.id.place_holder);
            dateTV = itemView.findViewById(R.id.date_holder);
            timeTV = itemView.findViewById(R.id.time_holder);
            context = itemView.getContext();
            Button btnMore = itemView.findViewById(R.id.btn_to_game);
            btnMore.setOnClickListener(this);
        }

        public void setGame(Game game) {
            this.game = game;
            setGameData();
        }

        private void setGameData() {
            setString(placeTV, context.getString(R.string.place), game.getPlace());
            setString(dateTV, context.getString(R.string.date), game.getDate().toString());
            setString(timeTV, context.getString(R.string.time), game.getTime().toString());
        }

        private void setString(TextView textView, String additional, String main) {
            String finalString = String.format(Locale.ENGLISH, additional, main);
            textView.setText(finalString);
        }

        @Override
        public void onClick(View view) {
            adapter.getActivity().setSelectedGame(game);
            adapter.getActivity().setRenderFragment(R.id.nav_game);
        }
    }
}

package ru.zvg.footballrate;

import android.content.Context;

import androidx.annotation.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SQLDataBase {
    private static final String IP = "192.168.0.104";
    private static final String PORT = "49172";
    private static final String DRIVER = "net.sourceforge.jtds.jdbc.Driver";
    private static final String DB = "FootballDB";
    private static final String USER = "test";
    private static final String PASSWORD = "test";
    private static final String URL = "jdbc:jtds:sqlserver://" + IP + ":" + PORT + "/" + DB;
    private static final String SELECT_LEAGUES = "EXEC select_league;";
    private static final String SELECT_TEAM = "EXEC select_team '%s';";
    private static final String SELECT_GAMES = "EXEC select_games;";
    public static final String ADD_ACC = "EXEC add_acc '%s', '%s', '%s'";
    public static final String ADD_RATE = "EXEC add_rate '%s', %d, %d, '%s'";
    public static final String CONTAINS_ACC = "EXEC contains_acc '%s', '%s';";
    public static final String VALID_ACC = "EXEC is_valid_acc_data '%s', '%s';";
    public static final String GET_RATE = "EXEC get_rates '%s';";
    private static SQLDataBase db;
    private List<Game> gameList;
    private List<League> leagueList;

    private SQLDataBase() {
        gameList = new ArrayList<>();
        leagueList = new ArrayList<>();
    }

    public static void createDataBase(Context context) {
        if (db != null) {
            return;
        }
        try (Connection connection = getConnection()) {
            db = new SQLDataBase();
            Statement statement = connection.createStatement();
            initLeagueList(statement);
            bindTeamList(statement);
            initGameList(statement);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastCreator.showToast(context, R.string.connection_exception);
            db = null;
        }
    }

    private static void initGameList(Statement statement) throws SQLException {
        ResultSet set = statement.executeQuery(SELECT_GAMES);
        while (set.next()) {
            Team teamLeft = db.getTeam(set.getString("team_left"));
            Team teamRight = db.getTeam(set.getString("team_right"));
            Game game = new Game(set.getInt("id"), teamLeft, teamRight, set.getString("place"));
            game.setTime(set.getTime("time"));
            game.setDate(set.getDate("date"));
            game.setCountLeft(set.getInt("count_left"));
            game.setCountRight(set.getInt("count_right"));
            db.getGameList().add(game);
        }
    }

    private static void bindTeamList(Statement statement) throws SQLException {
        for (League aLeague :
                db.getLeagueList()) {
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH, SELECT_TEAM, aLeague.getName()));
            while (set.next()) {
                Team team = new Team(set.getString("name"));
                team.setLogo(set.getBytes("logo"));
                aLeague.addTeam(team);
            }
        }
    }

    private static void initLeagueList(Statement statement) throws SQLException {
        ResultSet set = statement.executeQuery(SELECT_LEAGUES);
        while (set.next()) {
            League league = new League(set.getString("name"), set.getString("country"));
            league.setLogo(set.getBytes("file_stream"));
            db.leagueList.add(league);
        }
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public List<League> getLeagueList() {
        return leagueList;
    }

    public Team getTeam(String name) {
        for (League aLeague :
                db.getLeagueList()) {
            for (Team aTeam :
                    aLeague.getTeamList()) {
                if (aTeam.getName().equals(name)) {
                    return aTeam;
                }
            }
        }
        return null;
    }

    public static SQLDataBase getDb() {
        return db;
    }

    public Game getGame(int id) {
        for (Game aGame :
                db.getGameList()) {
            if (aGame.getId() == id) {
                return aGame;
            }
        }
        return null;
    }
}
